package io.sicredi.accounting.loader.mapping;

import io.sicredi.accounting.loader.domain.Balance;
import io.sicredi.accounting.loader.domain.Batch;
import io.sicredi.accounting.loader.domain.EngineLoaderOracleError;
import io.sicredi.accounting.loader.domain.TopazMovement;
import io.sicredi.accounting.loader.messaging.dto.BalanceDTO;
import io.sicredi.accounting.loader.messaging.dto.MovementDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@Component
public class AnalyticalMapping {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticalMapping.class);

    private static final Integer MAXIMUM_LENGTH_ITEM_TEXT = 45;

    public Optional<EngineLoaderOracleError> createEngineLoaderOracleError(String json, Exception exception) {
        LOGGER.debug("Criando engine loader oracle error");

        return Optional.ofNullable(exception)
                .map(ex -> {
                    final EngineLoaderOracleError engineLoaderOracleError = new EngineLoaderOracleError();

                    engineLoaderOracleError.setDetails(json);
                    engineLoaderOracleError.setMessage(Arrays.toString(ex.getStackTrace()));

                    return engineLoaderOracleError;
                });
    }

    public Optional<Batch> createBatch(Integer company, LocalDate now, Integer status, LocalDate baseDate, Integer loadType, String system) {
        LOGGER.debug("Criando carga no mapeador");

        final Batch load = new Batch();

        load.setCompany(company);
        load.setLoadDate(now);
        load.setStatus(status);
        load.setBaseDate(baseDate);
        load.setLoadType(loadType);
        load.setSystem(system);

        return Optional.of(load);
    }

    public Optional<Balance> dtoToBalance(BalanceDTO dto, String incomingUniqueEventId, Long topicPostingTimestamp) {
        LOGGER.debug("Transformando DTO para saldo");

        return Optional.ofNullable(dto)
                .map(d -> {
                    final Balance balance = new Balance();

                    balance.setAccount(dto.getAccount());

                    if (dto.getCompany() != null)
                        balance.setBaseDate(dto.getBaseDate().toLocalDate());

                    balance.setBaseDateTimestamp(dto.getBaseDate());

                    balance.setCompany(dto.getCompany());
                    balance.setIntegrate(dto.getIntegrate());
                    balance.setNature(dto.getNature());
                    balance.setProfitCenter(dto.getProfitCenter());
                    balance.setValue(dto.getValue());
                    balance.setIncomingUniqueEventId(incomingUniqueEventId);
                    balance.setDocument(dto.getDocument());

                    return balance;
                });
    }

    public Optional<TopazMovement> dtoToTopazMovement(MovementDTO dto, String incomingUniqueEventId, Long topicPostingTimestamp) {
        LOGGER.debug("Transformando DTO para movimento");

        return Optional.ofNullable(dto)
                .map(d -> {
                    final TopazMovement topazMovement = new TopazMovement();

                    topazMovement.setCompany(dto.getCompany());

                    if (dto.getBaseDate() != null)
                        topazMovement.setBaseDate(dto.getBaseDate().toLocalDate());

                    if (dto.getPostingDate() != null)
                        topazMovement.setPostingDate(dto.getPostingDate().toLocalDate());

                    topazMovement.setBaseDateTimestamp(dto.getBaseDate());
                    topazMovement.setPostingDateTimestamp(dto.getPostingDate());
                    topazMovement.setOperationDateTimestamp(dto.getOperationDate());

                    topazMovement.setFiscalYear(dto.getFiscalYear());
                    topazMovement.setFiscalPeriod(dto.getFiscalPeriod());
                    topazMovement.setDocType(dto.getSystemSapCode());
                    topazMovement.setRefDocNo(dto.getTransactionDetail());
                    topazMovement.setDocTopaz(dto.getTransactionId());

                    topazMovement.setGlAccount(dto.getScriptDebitAccount());

                    if (dto.getScriptDebitAccount() == null)
                        topazMovement.setGlAccount(dto.getScriptCreditAccount());

                    if (dto.getItemText() != null && !dto.getItemText().trim().isEmpty() && dto.getItemText().length() > MAXIMUM_LENGTH_ITEM_TEXT)
                        topazMovement.setItemText(dto.getItemText().substring(0, MAXIMUM_LENGTH_ITEM_TEXT)); //max 50 characters
                    else
                        topazMovement.setItemText(dto.getItemText());

                    topazMovement.setDebit(dto.getDebit());

                    if (dto.getDebit() == null)
                        topazMovement.setDebit(dto.getCredit());

                    topazMovement.setDebitUnit(dto.getDebitUnit());

                    if (dto.getDebitUnit() == null)
                        topazMovement.setDebitUnit(dto.getCreditUnit());

                    topazMovement.setDocument(dto.getDocument());
                    topazMovement.setDebitValue(dto.getDebitValue());

                    if (dto.getDebitValue() == null)
                        topazMovement.setDebitValue(dto.getCreditValue());

                    topazMovement.setIntegrate(dto.getIntegrate());
                    topazMovement.setNonCooperativeAct(dto.getNonCooperativeAct());

                    if (dto.getOperationDate() != null)
                        topazMovement.setOperationDate(dto.getOperationDate().toLocalDate());

                    topazMovement.setIncomingUniqueEventId(incomingUniqueEventId);

                    return topazMovement;
                });
    }

}
