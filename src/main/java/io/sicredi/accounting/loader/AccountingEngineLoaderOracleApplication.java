package io.sicredi.accounting.loader;

import io.sicredi.accounting.loader.messaging.processor.AnalyticalProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Profile;

@Profile("default")
@SpringBootApplication
@EnableBinding(AnalyticalProcessor.class)
public class AccountingEngineLoaderOracleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountingEngineLoaderOracleApplication .class, args);
	}

}
