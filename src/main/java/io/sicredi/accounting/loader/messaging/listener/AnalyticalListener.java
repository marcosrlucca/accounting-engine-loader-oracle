package io.sicredi.accounting.loader.messaging.listener;

import io.sicredi.accounting.loader.messaging.dto.AnalyticalDTO;
import io.sicredi.accounting.loader.messaging.processor.AnalyticalProcessor;
import io.sicredi.accounting.loader.service.AnalyticalService;
import io.sicredi.accounting.loader.service.EngineLoaderOracleErrorService;
import io.sicredi.accounting.loader.util.PayloadExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class AnalyticalListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticalListener.class);

    private final AnalyticalService analyticalService;
    private final EngineLoaderOracleErrorService engineLoaderOracleErrorService;

    private final PayloadExtractor extractor;

    @Autowired
    public AnalyticalListener(AnalyticalService analyticalService, EngineLoaderOracleErrorService engineLoaderOracleErrorService, PayloadExtractor extractor) {
        this.analyticalService = analyticalService;
        this.engineLoaderOracleErrorService = engineLoaderOracleErrorService;
        this.extractor = extractor;
    }

    @StreamListener(value = AnalyticalProcessor.INPUT)
    public void notification(Message<AnalyticalDTO> message) {
        LOGGER.debug("Mensagem recebida");

        try {
            extractor.extractDto(message)
                    .ifPresent(analyticalService::process);

            LOGGER.debug("Processameto concluído");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            dealException(message, ex);
        }
    }

    private void dealException(Message<AnalyticalDTO> message, Exception ex) {
        extractor.extractJson(message)
                .ifPresentOrElse(
                        json -> engineLoaderOracleErrorService.handleError(json, ex),
                        () -> engineLoaderOracleErrorService.handleError(ex));
    }

}
