package io.sicredi.accounting.loader.messaging.dto;

import java.util.List;

public class AnalyticalDTO extends EventDetailsDTO {

    private List<List<MovementDTO>> movements;
    private BalanceDTO balance;

    public List<List<MovementDTO>> getMovements() {
        return movements;
    }

    public void setMovements(List<List<MovementDTO>> movements) {
        this.movements = movements;
    }

    public BalanceDTO getBalance() {
        return balance;
    }

    public void setBalance(BalanceDTO balance) {
        this.balance = balance;
    }

}
