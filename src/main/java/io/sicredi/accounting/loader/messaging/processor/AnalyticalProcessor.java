package io.sicredi.accounting.loader.messaging.processor;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface AnalyticalProcessor {

    String INPUT = "analytical-processing-input";

    @Input(INPUT)
    SubscribableChannel analyticalProcessingInput();

}
