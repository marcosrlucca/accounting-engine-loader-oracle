package io.sicredi.accounting.loader.messaging.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class MovementDTO {

    private BigDecimal creditValue;

    private Boolean nonCooperativeAct;

    private BigDecimal debitValue;

    private String company;

    private ZonedDateTime operationDate;

    private String credit;

    private String debit;

    private ZonedDateTime baseDate;

    private String transactionId;

    private String systemSapCode;

    private String document;

    private Integer fiscalYear;

    private Integer fiscalPeriod;

    private String scriptDebitAccount;

    private String scriptCreditAccount;

    private String eventParameterId;

    private String integrate;

    private String itemText;

    private ZonedDateTime postingDate;

    private String transactionDetail;

    private String creditUnit;

    private String debitUnit;

    public BigDecimal getCreditValue() {
        return creditValue;
    }

    public void setCreditValue(BigDecimal creditValue) {
        this.creditValue = creditValue;
    }

    public Boolean getNonCooperativeAct() {
        return nonCooperativeAct;
    }

    public void setNonCooperativeAct(Boolean nonCooperativeAct) {
        this.nonCooperativeAct = nonCooperativeAct;
    }

    public BigDecimal getDebitValue() {
        return debitValue;
    }

    public void setDebitValue(BigDecimal debitValue) {
        this.debitValue = debitValue;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public ZonedDateTime getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(ZonedDateTime operationDate) {
        this.operationDate = operationDate;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public ZonedDateTime getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(ZonedDateTime baseDate) {
        this.baseDate = baseDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSystemSapCode() {
        return systemSapCode;
    }

    public void setSystemSapCode(String systemSapCode) {
        this.systemSapCode = systemSapCode;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Integer getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(Integer fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public Integer getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(Integer fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }

    public String getScriptDebitAccount() {
        return scriptDebitAccount;
    }

    public void setScriptDebitAccount(String scriptDebitAccount) {
        this.scriptDebitAccount = scriptDebitAccount;
    }

    public String getScriptCreditAccount() {
        return scriptCreditAccount;
    }

    public void setScriptCreditAccount(String scriptCreditAccount) {
        this.scriptCreditAccount = scriptCreditAccount;
    }

    public String getEventParameterId() {
        return eventParameterId;
    }

    public void setEventParameterId(String eventParameterId) {
        this.eventParameterId = eventParameterId;
    }

    public String getIntegrate() {
        return integrate;
    }

    public void setIntegrate(String integrate) {
        this.integrate = integrate;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }

    public ZonedDateTime getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(ZonedDateTime postingDate) {
        this.postingDate = postingDate;
    }

    public String getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(String transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public String getCreditUnit() {
        return creditUnit;
    }

    public void setCreditUnit(String creditUnit) {
        this.creditUnit = creditUnit;
    }

    public String getDebitUnit() {
        return debitUnit;
    }

    public void setDebitUnit(String debitUnit) {
        this.debitUnit = debitUnit;
    }
}
