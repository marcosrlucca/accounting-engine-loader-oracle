package io.sicredi.accounting.loader.messaging.dto;

public class EventDetailsDTO {

    private String incomingUniqueEventIdDB;
    private Long topicPostingTimestamp;

    public String getIncomingUniqueEventIdDB() {
        return incomingUniqueEventIdDB;
    }

    public void setIncomingUniqueEventIdDB(String incomingUniqueEventIdDB) {
        this.incomingUniqueEventIdDB = incomingUniqueEventIdDB;
    }

    public Long getTopicPostingTimestamp() {
        return topicPostingTimestamp;
    }

    public void setTopicPostingTimestamp(Long topicPostingTimestamp) {
        this.topicPostingTimestamp = topicPostingTimestamp;
    }
}
