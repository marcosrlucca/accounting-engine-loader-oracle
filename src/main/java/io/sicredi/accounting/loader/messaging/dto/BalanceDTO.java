package io.sicredi.accounting.loader.messaging.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class BalanceDTO {

    private ZonedDateTime baseDate;

    private String profitCenter;

    private String company;

    private String account;

    private String nature;

    private BigDecimal value;

    private String integrate;

    private String systemSapCode;

    private String document;

    public ZonedDateTime getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(ZonedDateTime baseDate) {
        this.baseDate = baseDate;
    }

    public String getProfitCenter() {
        return profitCenter;
    }

    public void setProfitCenter(String profitCenter) {
        this.profitCenter = profitCenter;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getIntegrate() {
        return integrate;
    }

    public void setIntegrate(String integrate) {
        this.integrate = integrate;
    }

    public String getSystemSapCode() {
        return systemSapCode;
    }

    public void setSystemSapCode(String systemSapCode) {
        this.systemSapCode = systemSapCode;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
