package io.sicredi.accounting.loader.repository;

import io.sicredi.accounting.loader.domain.Balance;
import org.springframework.data.repository.CrudRepository;

public interface BalanceRepository extends CrudRepository<Balance, Long>, BalanceRepositoryCustom {

}
