package io.sicredi.accounting.loader.repository;

import java.time.ZonedDateTime;

public interface BalanceRepositoryCustom {

    int updateAllPreviousByDocumentToDisconsidered(Long batchId, String account, String document, ZonedDateTime baseDate);

}
