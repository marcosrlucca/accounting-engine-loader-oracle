package io.sicredi.accounting.loader.repository;

import io.sicredi.accounting.loader.domain.EngineLoaderOracleError;
import org.springframework.data.repository.CrudRepository;
import java.time.ZonedDateTime;

public interface EngineLoaderOracleErrorRepository extends CrudRepository<EngineLoaderOracleError, ZonedDateTime> {

}
