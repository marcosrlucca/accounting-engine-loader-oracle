package io.sicredi.accounting.loader.repository;

import io.sicredi.accounting.loader.domain.SystemParameter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
public interface SystemParameterRepository extends CrudRepository<SystemParameter, Long> {

    @Query(value = "select * from CONTAB_OWNER.EC_PAR_SISTEMA where codigo_sap = :codigo_sap", nativeQuery = true)
    Optional<SystemParameter> findByCode(@Param("codigo_sap") String sapCode);
}
