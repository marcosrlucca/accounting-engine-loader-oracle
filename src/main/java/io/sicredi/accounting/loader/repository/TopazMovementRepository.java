package io.sicredi.accounting.loader.repository;

import io.sicredi.accounting.loader.domain.TopazMovement;
import org.springframework.data.repository.CrudRepository;

public interface TopazMovementRepository extends CrudRepository<TopazMovement, Long> {

}
