package io.sicredi.accounting.loader.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.ZonedDateTime;
import java.util.Optional;

public class BalanceRepositoryCustomImpl implements BalanceRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings({"SqlDialectInspection", "JpaQueryApiInspection"})
    @Override
    public int updateAllPreviousByDocumentToDisconsidered(Long batchId, String account, String document, ZonedDateTime baseDate) {
        String sql =
                "UPDATE contab_owner.ic_saldo " +
                        "SET " +
                        "    desconsiderar = 1 " +
                        "WHERE " +
                        "    incoming_unique_event_id_db " +
                        "IN " +
                        "   ( " +
                        "   SELECT " +
                        "        s.incoming_unique_event_id_db AS unique_index " +
                        "   FROM " +
                        "      contab_owner.ic_saldo s " +
                        "        INNER JOIN contab_owner.ic_carga c ON ( s.id_carga = c.id ) " +
                        "   WHERE " +
                        "        s.incoming_unique_event_id_db IS NOT NULL " +
                        "       AND s.datum <= :baseDate " +
                        "       AND c.id = :batchId" +
                        "       AND s.racct = :account";

        if( Optional.ofNullable(document).isEmpty() ) {
            sql += "            AND s.documento IS NULL ";
        } else {
            sql += "            AND s.documento = :document ";
        }
        sql += "            ) ";

        if( Optional.ofNullable(document).isEmpty() ) {
            sql += "            AND documento IS NULL ";
        } else {
            sql += "            AND documento = :document ";
        }

        final Query nativeQuery = em.createNativeQuery(sql);

        Optional.ofNullable(baseDate).ifPresent(d -> nativeQuery.setParameter("baseDate", d));
        Optional.ofNullable(batchId).ifPresent(b -> nativeQuery.setParameter("batchId", b));
        Optional.ofNullable(account).ifPresent(b -> nativeQuery.setParameter("account", b));
        if( !Optional.ofNullable(document).isEmpty() ) {
            Optional.ofNullable(document).ifPresent(doc -> nativeQuery.setParameter("document", doc));
        }

        return nativeQuery.executeUpdate();
    }
}
