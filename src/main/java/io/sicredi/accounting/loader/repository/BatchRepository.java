package io.sicredi.accounting.loader.repository;

import io.sicredi.accounting.loader.domain.Batch;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
public interface BatchRepository extends CrudRepository<Batch, Long> {

    @Query(value = "select * from CONTAB_OWNER.ic_carga carga where empresa = :company and data_base = :baseDate and tipo_carga = :loadType and sistema = :system", nativeQuery = true)
    Optional<List<Batch>> findBy(@Param("company") Integer company,
                                 @Param("baseDate") LocalDate dataBase,
                                 @Param("loadType") Integer loadType,
                                 @Param("system") String system);

}
