package io.sicredi.accounting.loader.exception;

public class DuplicatedException extends RuntimeException {

    public DuplicatedException(String message) {
        super(message);
    }

    public DuplicatedException() {
        super();
    }

}
