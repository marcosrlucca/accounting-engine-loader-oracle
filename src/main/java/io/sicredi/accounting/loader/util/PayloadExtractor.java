package io.sicredi.accounting.loader.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.GsonBuilder;
import io.sicredi.accounting.loader.messaging.dto.AnalyticalDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PayloadExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayloadExtractor.class);

    private final ObjectMapper objectMapper;

    @Autowired
    public PayloadExtractor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Optional<AnalyticalDTO> extractDto(Message<AnalyticalDTO> message) {
        LOGGER.debug("Extraindo payload da mensagem");

        return Optional.ofNullable(message)
                .map(Message::getPayload);
    }

    public Optional<String> extractJson(Message<AnalyticalDTO> message) {
        LOGGER.debug("Extraindo payload da mensagem para transformar em Json");

        try {
            return Optional.ofNullable(this.objectMapper.writeValueAsString(message.getPayload()));

        } catch (Throwable t) {
            LOGGER.error(t.getMessage(), t);

            return Optional.empty();
        }
    }
}
