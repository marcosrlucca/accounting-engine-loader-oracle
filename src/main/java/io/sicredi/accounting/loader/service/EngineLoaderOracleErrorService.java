package io.sicredi.accounting.loader.service;

import io.sicredi.accounting.loader.mapping.AnalyticalMapping;
import io.sicredi.accounting.loader.repository.EngineLoaderOracleErrorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EngineLoaderOracleErrorService {

    private final EngineLoaderOracleErrorRepository repository;

    private final AnalyticalMapping mapping;

    @Autowired
    public EngineLoaderOracleErrorService(EngineLoaderOracleErrorRepository repository, AnalyticalMapping mapping) {
        this.repository = repository;
        this.mapping = mapping;
    }

    public void handleError(String json, Exception exception) {
        mapping.createEngineLoaderOracleError(json, exception)
                .ifPresent(repository::save);
    }

    public void handleError(Exception exception) {
        mapping.createEngineLoaderOracleError(null, exception)
                .ifPresent(repository::save);
    }
}
