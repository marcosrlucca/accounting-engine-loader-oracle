package io.sicredi.accounting.loader.service;

import io.sicredi.accounting.loader.domain.Balance;
import io.sicredi.accounting.loader.domain.Batch;
import io.sicredi.accounting.loader.domain.SystemParameter;
import io.sicredi.accounting.loader.exception.BusinessException;
import io.sicredi.accounting.loader.mapping.AnalyticalMapping;
import io.sicredi.accounting.loader.messaging.dto.BalanceDTO;
import io.sicredi.accounting.loader.repository.BalanceRepository;
import io.sicredi.accounting.loader.repository.SystemParameterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static io.sicredi.accounting.loader.service.BatchService.TYPE_BALANCE;

@Service
public class BalanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceService.class);

    private final AnalyticalMapping analyticalMapping;

    private final BalanceRepository balanceRepository;
    private final SystemParameterRepository systemParameterRepository;

    private final BatchService batchService;

    @Autowired
    public BalanceService(AnalyticalMapping analyticalMapping, BalanceRepository balanceRepository, SystemParameterRepository systemParameterRepository, BatchService batchService) {
        this.analyticalMapping = analyticalMapping;
        this.balanceRepository = balanceRepository;
        this.systemParameterRepository = systemParameterRepository;
        this.batchService = batchService;
    }

    public void process(BalanceDTO dto, String incomingUniqueEventIdDB, Long topicPostingTimestamp) {
        LOGGER.debug("Iniciando serviço de saldo");

        final Optional<Balance> maybeBalance = analyticalMapping.dtoToBalance(dto, incomingUniqueEventIdDB, topicPostingTimestamp);

        final Optional<SystemParameter> maybeSystem = systemParameterRepository.findByCode(dto.getSystemSapCode());

        maybeBalance
                .ifPresentOrElse(balance -> maybeSystem
                                .ifPresentOrElse(system -> {
                                            LOGGER.debug("Procurando carga por empresa, data base, tipo saldo e sistema");

                                            final Optional<Batch> maybeBatch = batchService.findOneByOrCreate(Integer.valueOf(balance.getCompany()), balance.getBaseDate(), TYPE_BALANCE, system.getSystem());

                                            maybeBatch
                                                    .ifPresentOrElse(batch -> {
                                                                LOGGER.debug("Salvando saldo");

                                                                balance.setBatchId(batch.getBatchId());
                                                                balance.setDisconsider(Boolean.FALSE);

                                                                //se existe saldos com data/hora anterior ao atual, atualiza eles para serem desconsiderados
                                                                int result =
                                                                        balanceRepository.updateAllPreviousByDocumentToDisconsidered(
                                                                                batch.getBatchId(), balance.getAccount(), balance.getDocument(), balance.getBaseDateTimestamp());

                                                                balanceRepository.save(balance);
                                                            },
                                                            () -> new BusinessException("Could not create batch"));
                                        },
                                        () -> new BusinessException("Could not find system")),
                        () -> new BusinessException("Could not map DTO"));
    }

}
