package io.sicredi.accounting.loader.service;

import io.sicredi.accounting.loader.domain.Batch;
import io.sicredi.accounting.loader.mapping.AnalyticalMapping;
import io.sicredi.accounting.loader.repository.BatchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class BatchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchService.class);

    private static final Integer STATUS_MUST_PROCESS = 1;

    static final Integer TYPE_MOVEMENT = 1;
    static final Integer TYPE_BALANCE = 2;

    private final AnalyticalMapping analyticalMapping;

    private final BatchRepository batchRepository;

    @Autowired
    public BatchService(AnalyticalMapping analyticalMapping, BatchRepository batchRepository) {
        this.analyticalMapping = analyticalMapping;
        this.batchRepository = batchRepository;
    }

    public Optional<Batch> findOneByOrCreate(Integer company, LocalDate baseDate, Integer loadType, String system) {
        LOGGER.debug("Iniciando serviço de carga");

        final LocalDate now = LocalDate.now();

        final Optional<List<Batch>> maybeBatches = batchRepository.findBy(company, baseDate, loadType, system);

        return maybeBatches
                .flatMap(batches -> {
                    LOGGER.debug("Carga encontrada");

                    return batches.stream().findFirst();
                })
                .or(() -> {
                    LOGGER.debug("Carga não encontrada");

                    final Optional<Batch> maybeNewBatch = analyticalMapping.createBatch(company, now, STATUS_MUST_PROCESS, baseDate, loadType, system);

                    LOGGER.debug("Criando nova carga");

                    return maybeNewBatch
                            .map(batchRepository::save);
                });
    }

}
