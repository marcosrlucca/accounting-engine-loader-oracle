package io.sicredi.accounting.loader.service;

import io.sicredi.accounting.loader.domain.Batch;
import io.sicredi.accounting.loader.domain.SystemParameter;
import io.sicredi.accounting.loader.domain.TopazMovement;
import io.sicredi.accounting.loader.exception.BusinessException;
import io.sicredi.accounting.loader.mapping.AnalyticalMapping;
import io.sicredi.accounting.loader.messaging.dto.MovementDTO;
import io.sicredi.accounting.loader.repository.SystemParameterRepository;
import io.sicredi.accounting.loader.repository.TopazMovementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static io.sicredi.accounting.loader.service.BatchService.TYPE_MOVEMENT;

@Service
public class MovementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovementService.class);

    private final AnalyticalMapping analyticalMapping;

    private final TopazMovementRepository topazMovementRepository;
    private final SystemParameterRepository systemParameterRepository;

    private final BatchService batchService;

    @Autowired
    public MovementService(AnalyticalMapping analyticalMapping, TopazMovementRepository topazMovementRepository, SystemParameterRepository systemParameterRepository, BatchService batchService) {
        this.analyticalMapping = analyticalMapping;
        this.topazMovementRepository = topazMovementRepository;
        this.systemParameterRepository = systemParameterRepository;
        this.batchService = batchService;
    }

    public void process(List<List<MovementDTO>> movementsDTO, String incomingUniqueEventId, Long topicPostingTimestamp) {
        LOGGER.debug("Iniciando serviço de movimento");

        final Optional<Batch> maybeBatchId = findBatch(movementsDTO);

        maybeBatchId
                .ifPresentOrElse(batch -> {
                            movementsDTO
                                    .stream()
                                    .filter(movementDTO -> !movementDTO.isEmpty())
                                    .filter(movementDTO -> movementDTO.size() == 2) //one debit and one credit
                                    .forEach(movementDTO -> processDebitAndCredit(movementDTO.get(0), movementDTO.get(1), incomingUniqueEventId, batch.getBatchId(), topicPostingTimestamp));
                        },
                        () -> new BusinessException("Could not find batch"));
    }

    private void processDebitAndCredit(MovementDTO movementDTO1, MovementDTO movementDTO2, String incomingUniqueEventIdDB, Long batchId, Long topicPostingTimestamp) {
        final Optional<TopazMovement> maybeMovement1 = analyticalMapping.dtoToTopazMovement(movementDTO1, incomingUniqueEventIdDB, topicPostingTimestamp);
        final Optional<TopazMovement> maybeMovement2 = analyticalMapping.dtoToTopazMovement(movementDTO2, incomingUniqueEventIdDB, topicPostingTimestamp);

        maybeMovement1
                .ifPresentOrElse(movement1 -> maybeMovement2
                                .ifPresentOrElse(movement2 -> {
                                            LOGGER.debug("Mandando processar débito e crédito");

                                            processDebitOrCredit(movement1, batchId);
                                            processDebitOrCredit(movement2, batchId);
                                        },
                                        () -> new BusinessException("Movement 2 can not be empty")),
                        () -> new BusinessException("Movement 1 can not be empty"));
    }

    private void processDebitOrCredit(TopazMovement movementDebitOrCredit, Long batchId) {
        LOGGER.debug("Salvando movimento de débito ou crédito");

        movementDebitOrCredit.setBatchId(batchId);

        topazMovementRepository.save(movementDebitOrCredit);
    }

    private Optional<Batch> findBatch(List<List<MovementDTO>> movementsDTO) {
        return movementsDTO
                .stream()
                .map(movementDTO -> movementDTO
                        .stream()
                        .findFirst()
                        .map(MovementDTO::getBaseDate)
                        .map(ZonedDateTime::toLocalDate)
                        .flatMap(baseDate -> movementDTO
                                .stream()
                                .findFirst()
                                .map(MovementDTO::getCompany)
                                .map(Integer::valueOf)
                                .flatMap(company -> movementDTO
                                        .stream()
                                        .findFirst()
                                        .map(MovementDTO::getSystemSapCode)
                                        .flatMap(systemParameterRepository::findByCode)
                                        .map(SystemParameter::getSystem)
                                        .flatMap(system -> batchService.findOneByOrCreate(company, baseDate, TYPE_MOVEMENT, system))))
                        .orElseThrow(() -> new BusinessException("Could not select any batch")))
                .findFirst();
    }

}
