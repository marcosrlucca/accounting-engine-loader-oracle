package io.sicredi.accounting.loader.service;

import io.sicredi.accounting.loader.messaging.dto.AnalyticalDTO;
import io.sicredi.accounting.loader.messaging.dto.MovementDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class AnalyticalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticalService.class);

    private final MovementService movementService;
    private final BalanceService balanceService;

    @Autowired
    public AnalyticalService(MovementService movementService, BalanceService balanceService) {
        this.movementService = movementService;
        this.balanceService = balanceService;
    }

    public void process(AnalyticalDTO dto) {
        LOGGER.debug("Iniciando serviço analítico");

        final Optional<AnalyticalDTO> maybeMovementOrBalance = Optional.of(dto);

        maybeMovementOrBalance
                .ifPresent(movementOrBalance -> {
                    final Optional<List<List<MovementDTO>>> maybeMovement = Optional.ofNullable(movementOrBalance.getMovements() == null || movementOrBalance.getMovements().isEmpty() ? null : movementOrBalance.getMovements());

                    if (maybeMovement.isPresent()) {
                        LOGGER.debug("Processar usando serviço de movimento");

                        movementService.process(maybeMovement.get(), movementOrBalance.getIncomingUniqueEventIdDB(), movementOrBalance.getTopicPostingTimestamp());
                    } else {
                        LOGGER.debug("Processar usando serviço de saldo");

                        balanceService.process(movementOrBalance.getBalance(), movementOrBalance.getIncomingUniqueEventIdDB(), movementOrBalance.getTopicPostingTimestamp());
                    }
                });
    }

}
