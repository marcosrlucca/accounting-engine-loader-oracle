package io.sicredi.accounting.loader.domain;

import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "IC_CARGA", schema = "CONTAB_OWNER")
public class Batch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long batchId;

    @Column(name = "EMPRESA", nullable = false)
    private Integer company;

    @Column(name = "DATA_CARGA", nullable = false)
    private LocalDate loadDate;

    @Column(name = "STATUS", nullable = false)
    private Integer status;

    @Column(name = "DATA_BASE", nullable = false)
    private LocalDate baseDate;

    @Column(name = "TIPO_CARGA", nullable = false)
    private Integer loadType;

    @Column(name = "SISTEMA")
    @Length(max = 20)
    private String system;

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    public LocalDate getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(LocalDate loadDate) {
        this.loadDate = loadDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDate getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(LocalDate baseDate) {
        this.baseDate = baseDate;
    }

    public Integer getLoadType() {
        return loadType;
    }

    public void setLoadType(Integer loadType) {
        this.loadType = loadType;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }
}
