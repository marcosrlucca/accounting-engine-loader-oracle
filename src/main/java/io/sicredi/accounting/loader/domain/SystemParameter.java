package io.sicredi.accounting.loader.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "EC_PAR_SISTEMA", schema = "CONTAB_OWNER")
public class SystemParameter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SISTEMA")
    private Long systemId;

    @Column(name = "SISTEMA")
    private String system;

    @Column(name = "DESCRICAO_SISTEMA")
    private String systemDescription;

    @Column(name = "EMAIL_RESPONSAVEL")
    private String ownerEmail;

    @Column(name = "INICIO_VALIDADE")
    private LocalDate startDate;

    @Column(name = "FIM_VALIDADE")
    private LocalDate endDate;

    @Column(name = "RESPONSAVEL_CADASTRO")
    private String ownerRegistration;

    @Column(name = "CODIGO_SAP")
    private String sapCode;

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSystemDescription() {
        return systemDescription;
    }

    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getOwnerRegistration() {
        return ownerRegistration;
    }

    public void setOwnerRegistration(String ownerRegistration) {
        this.ownerRegistration = ownerRegistration;
    }

    public String getSapCode() {
        return sapCode;
    }

    public void setSapCode(String sapCode) {
        this.sapCode = sapCode;
    }
}
