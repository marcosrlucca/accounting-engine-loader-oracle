package io.sicredi.accounting.loader.domain;

import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

class TopazMovementCompositeKey implements Serializable {
    private Long batchId;
    private String company;
    private LocalDate baseDate;
    private LocalDate postingDate;
    private Integer fiscalYear;
    private Integer fiscalPeriod;
    private String docType;
    private String refDocNo;
    private String docTopaz;
    private String glAccount;
    private String itemText;
    private BigDecimal debitValue;
    private String debitUnit;
    private String debit;
    private String movementId;
    private String integrate;
    private String document;
    private LocalDate operationDate;
    private Boolean nonCooperativeAct;
    private String eventId;
    private String incomingUniqueEventId;
}

@Entity
@IdClass(TopazMovementCompositeKey.class)
@Table(name = "IC_MOVIMENTO_TOPAZ", schema = "CONTAB_OWNER")
public class TopazMovement {

    @Id
    @Column(name = "ID_CARGA", nullable = false)
    private Long batchId;

    @Id
    @Column(name = "COMP_CODE")
    @Length(max = 4)
    private String company;

    @Id
    @Column(name = "DOC_DATE")
    private LocalDate baseDate;

    @Column(name = "DOC_DATE_TS")
    private ZonedDateTime baseDateTimestamp;

    @Id
    @Column(name = "PSTNG_DATE")
    private LocalDate postingDate;

    @Column(name = "PSTNG_DATE_TS")
    private ZonedDateTime postingDateTimestamp;

    @Id
    @Column(name = "FISC_YEAR")
    private Integer fiscalYear;

    @Id
    @Column(name = "FIS_PERIOD")
    private Integer fiscalPeriod;

    @Id
    @Column(name = "DOC_TYPE")
    @Length(max = 2)
    private String docType;

    @Id
    @Column(name = "REF_DOC_NO")
    @Length(max = 60)
    private String refDocNo;

    @Id
    @Column(name = "DOC_TOPAZ")
    @Length(max = 50)
    private String docTopaz;

    @Id
    @Column(name = "GL_ACCOUNT")
    @Length(max = 17)
    private String glAccount;

    @Id
    @Column(name = "ITEM_TEXT")
    @Length(max = 50)
    private String itemText;

    @Id
    @Column(name = "AMT_DOCCUR")
    private BigDecimal debitValue;

    @Id
    @Column(name = "VALUEPART2")
    @Length(max = 240)
    private String debitUnit;

    @Id
    @Column(name = "DEBITOCREDITO")

    private String debit;

    @Id
    @Column(name = "ID_MOVIMENTO")
    private Long movementId;

    @Id
    @Column(name = "INTEGRAR")
    @Length(max = 1)
    private String integrate;

    @Id
    @Column(name = "DOCUMENTO")
    @Length(max = 14)
    private String document;

    @Id
    @Column(name = "DATA_OPERACAO")
    private LocalDate operationDate;

    @Column(name = "DATA_OPERACAO_TS")
    private ZonedDateTime operationDateTimestamp;

    @Id
    @Column(name = "ATONAOCOOPERATIVO")
    private Boolean nonCooperativeAct;

    @Id
    @Column(name = "ID_EVENTO")
    private Long eventId;

    @Id
    @Column(name = "INCOMING_UNIQUE_EVENT_ID_DB")
    @Length(max = 50)
    @NotEmpty
    private String incomingUniqueEventId;

    @Column(name = "DESCONSIDERAR")
    private Boolean disconsider;

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LocalDate getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(LocalDate baseDate) {
        this.baseDate = baseDate;
    }

    public ZonedDateTime getBaseDateTimestamp() {
        return baseDateTimestamp;
    }

    public void setBaseDateTimestamp(ZonedDateTime baseDateTimestamp) {
        this.baseDateTimestamp = baseDateTimestamp;
    }

    public LocalDate getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(LocalDate postingDate) {
        this.postingDate = postingDate;
    }

    public ZonedDateTime getPostingDateTimestamp() {
        return postingDateTimestamp;
    }

    public void setPostingDateTimestamp(ZonedDateTime postingDateTimestamp) {
        this.postingDateTimestamp = postingDateTimestamp;
    }

    public Integer getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(Integer fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public Integer getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(Integer fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getRefDocNo() {
        return refDocNo;
    }

    public void setRefDocNo(String refDocNo) {
        this.refDocNo = refDocNo;
    }

    public String getDocTopaz() {
        return docTopaz;
    }

    public void setDocTopaz(String docTopaz) {
        this.docTopaz = docTopaz;
    }

    public String getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(String glAccount) {
        this.glAccount = glAccount;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }

    public BigDecimal getDebitValue() {
        return debitValue;
    }

    public void setDebitValue(BigDecimal debitValue) {
        this.debitValue = debitValue;
    }

    public String getDebitUnit() {
        return debitUnit;
    }

    public void setDebitUnit(String debitUnit) {
        this.debitUnit = debitUnit;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public Long getMovementId() {
        return movementId;
    }

    public void setMovementId(Long movementId) {
        this.movementId = movementId;
    }

    public String getIntegrate() {
        return integrate;
    }

    public void setIntegrate(String integrate) {
        this.integrate = integrate;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public LocalDate getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(LocalDate operationDate) {
        this.operationDate = operationDate;
    }

    public ZonedDateTime getOperationDateTimestamp() {
        return operationDateTimestamp;
    }

    public void setOperationDateTimestamp(ZonedDateTime operationDateTimestamp) {
        this.operationDateTimestamp = operationDateTimestamp;
    }

    public Boolean getNonCooperativeAct() {
        return nonCooperativeAct;
    }

    public void setNonCooperativeAct(Boolean nonCooperativeAct) {
        this.nonCooperativeAct = nonCooperativeAct;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getIncomingUniqueEventId() {
        return incomingUniqueEventId;
    }

    public void setIncomingUniqueEventId(String incomingUniqueEventId) {
        this.incomingUniqueEventId = incomingUniqueEventId;
    }

    public Boolean getDisconsider() {
        return disconsider;
    }

    public void setDisconsider(Boolean disconsider) {
        this.disconsider = disconsider;
    }
}
