package io.sicredi.accounting.loader.domain;

import java.math.BigDecimal;

public interface Counting {

    public Integer getCounting();

    public void setCounting(Integer counting);

    public String getId();

    public void setId(String Id);

    public String getAccount();

    public void setAccount(String account);

    public String getText();

    public void setText(String text);

    public BigDecimal getVal();

    public void setVal(BigDecimal val);

}
