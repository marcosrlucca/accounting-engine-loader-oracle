package io.sicredi.accounting.loader.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

class BalanceCompositeKey implements Serializable {
    private Long batchId;
    private LocalDate baseDate;
    private String company;
    private String profitCenter;
    private String account;
    private String nature;
    private BigDecimal value;
    private String integrate;
    private String incomingUniqueEventId;
}

@Entity
@IdClass(BalanceCompositeKey.class)
@Table(name = "IC_SALDO", schema = "CONTAB_OWNER")
public class Balance {

    @Id
    @Column(name = "ID_CARGA", nullable = false)
    private Long batchId;

    @Id
    @Column(name = "DATUM")
    private LocalDate baseDate;

    @Id
    @Column(name = "DATUM_TS")
    private ZonedDateTime baseDateTimestamp;

    @Id
    @Column(name = "BUKRS")
    @Length(max = 4)
    private String company;

    @Id
    @Column(name = "PRCTR")
    @Length(max = 12)
    private String profitCenter;

    @Id
    @Column(name = "RACCT")
    @Length(max = 10)
    private String account;

    @Id
    @Column(name = "BSCHL")
    @Length(max = 1)
    private String nature;

    @Id
    @Column(name = "HSLXX")
    private BigDecimal value;

    @Id
    @Column(name = "INTEGRAR")
    @Length(max = 1)
    private String integrate;

    @Column(name = "ID_LOTE")
    private Long idLote;

    @Column(name = "HSLXX_INTEGRAR")
    private Long value2;

    @Id
    @Column(name = "INCOMING_UNIQUE_EVENT_ID_DB")
    @Length(max = 50)
    private String incomingUniqueEventId;

    @Column(name = "DESCONSIDERAR")
    private Boolean disconsider;

    @Column(name = "DOCUMENTO")
    private String document;

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public LocalDate getBaseDate() {
        return baseDate;
    }

    public void setBaseDate(LocalDate baseDate) {
        this.baseDate = baseDate;
    }

    public ZonedDateTime getBaseDateTimestamp() {
        return baseDateTimestamp;
    }

    public void setBaseDateTimestamp(ZonedDateTime baseDateTimestamp) {
        this.baseDateTimestamp = baseDateTimestamp;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProfitCenter() {
        return profitCenter;
    }

    public void setProfitCenter(String profitCenter) {
        this.profitCenter = profitCenter;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getIntegrate() {
        return integrate;
    }

    public void setIntegrate(String integrate) {
        this.integrate = integrate;
    }

    public Long getIdLote() {
        return idLote;
    }

    public void setIdLote(Long idLote) {
        this.idLote = idLote;
    }

    public Long getValue2() {
        return value2;
    }

    public void setValue2(Long value2) {
        this.value2 = value2;
    }

    public String getIncomingUniqueEventId() {
        return incomingUniqueEventId;
    }

    public void setIncomingUniqueEventId(String incomingUniqueEventId) {
        this.incomingUniqueEventId = incomingUniqueEventId;
    }

    public Boolean getDisconsider() {
        return disconsider;
    }

    public void setDisconsider(Boolean disconsider) {
        this.disconsider = disconsider;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
