package io.sicredi.accounting.loader.domain;

import javax.persistence.*;

@Entity
@Table(name = "ENGINE_LOADER_ORACLE_ERROR", schema = "CONTAB_OWNER")
public class EngineLoaderOracleError {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "DETAILS")
    private String details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
